#!/usr/bin/env node

import * as path from 'path';
import addUser from './adduser';

function main() {
    try {
        const [ user, passwd ] = process.argv.slice(2);
        console.log(addUser(user, passwd));
    } catch (e) {
        console.error(e);
        usage();
    }
}

function usage() {
    const bin = path.basename(process.argv[0]);
    let cmd;
    if (bin === 'node') {
        cmd = `${bin} ${path.relative(process.cwd(), process.argv[1])}`;
    } else {
        cmd = bin;
    }

    console.error(`Usage:\n    ${cmd} user passwd\n`);
}

main();
